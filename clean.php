<?php

if (php_sapi_name() !== 'cli') {
  print '<p>This is a command line only tool. Please run it from the console.</p>';
  print '<p>Usage: <strong>php clean.php file_to_clean.csv</strong></p>';
  exit;
}

if (!empty($argv) && empty($argv[1]))
{
  print "File argument missing.\n";
  print "Usage: php clean.php file_to_clean.csv\n";
  exit;
}

$cleanup = new Cleanup($argv[1]);
$cleanup->run();
$cleanup->write_file();


class Cleanup
{
  // Some defaults.
  private $filename;
  private $contents = array();
  private $coordinates = array();
  private $radius;
  private $unit;
  // Allowed speed between two points - average between points in points.csv 
  // is around 5, so it can be lowered even more to be more precise.
  private $allowed_speed = 7;
  // Predefined Constant.
  private $pi = M_PI;

  public function __construct($filename)
  {
    // Check if file exist
    if (file_exists($filename))
    {
      // On systems which differentiate between binary and text files 
      // (i.e. Windows) the file must be opened with 'b'.
      $lines  = file($filename);
      $this->filename = $filename;
      // Convert lines to some readable array.
      foreach ($lines  as $i => $line) {
        $pieces = explode(',', $line);
        $this->contents[$i] = array(
          'lat' => $pieces[0],
          'long' => $pieces[1],
          'time' => trim($pieces[2]),
        );
      }
      // Set default variables (km - kilometers; mi - miles).
      $this->set_units('km');
    }
    else
    {
      print 'Error opening file: ' . $filename . "\n";
      exit;
    }
  }

  public function write_file() {
    $file = explode('.', $this->filename);
    $filename = $file[0] . '_clean.' . $file[1];
  
    $fp = fopen($filename, 'w');
    foreach ($this->coordinates as $i => $entry) {
      $line = implode(',', $entry);
      fwrite($fp, $line . "\n");
    }
    fclose($fp);
    print 'Clean file saved as: ' . $filename . "\n";
  }

  public function run()
  {
    // Holder for proper last known coordinate, so if there is a point to skip
    // it won't be assigned to this variable.
    $last = null;
    foreach ($this->contents as $i => $entry) {
      // Assume that first coordinate is correct - it's a starting point, add it
      // to correct points array, and skip it.
      if ($i == 0)
      {
        $this->coordinates[] = $entry;
        $last = $entry;
      }
      else
      {
        // Check if distance between points could be driven with a specified
        // allowed speed (default 30km/h).
        $distance = $this->get_distance($last, $entry) * 100;
        $duration = $this->get_duration($last, $entry);
        $speed = ceil($distance / $duration / $this->unit * 3600);
        // If speed is too hight - skip this point.
        if ($speed > $this->allowed_speed)
        {
          print 'Skipped coordinates from line ' . $i . "\n";
        }
        else
        {
          $this->coordinates[] = $entry;
          $last = $entry;
        }
      }
    }
  }

  private function get_distance($point1, $point2)
  {
    if (!empty($point1) && !empty($point2) && !empty($point1['lat']) && !empty($point2['lat']) && !empty($point1['long']) && !empty($point2['long']))
    { 
        $pi180 = 180 / $this->pi; 
        $dist = acos(sin($point1['lat']/$pi180) * sin($point2['lat']/$pi180) + cos($point1['lat']/$pi180) * cos($point2['lat']/$pi180) * cos($point2['long']/$pi180 - $point1['long']/$pi180)) * $this->radius; 
        return $dist; 
    }
    return 0;
  }

  private function get_duration($point1, $point2)
  {
    if (!empty($point1) && !empty($point2) && !empty($point1['time']) && !empty($point2['time']))
    { 
        $diff = $point1['time'] - $point2['time'];
        return abs($diff); 
    }
    return 0;
  }

  private function set_units($unit = 'km')
  { 
    $this->unit = $unit; 
    switch($unit)
    {
      // Kilometers
      case 'km':
        $this->radius = 6378.137;
        $this->unit = 1000;
        break;
      // Miles
      case 'mi':
        $this->radius = 3963.191;
        $this->unit = 1609.344;
        break;
    }
  }
}

?>